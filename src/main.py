import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtWidgets
from app import MainWindow


def main():
  qt_app = pg.mkQApp()
  win = MainWindow()
  QtCore.QTimer.singleShot(0, win.showMaximized)
  pg.exec()
  
def main_script():
  qt_app = QtWidgets.QApplication.instance() or QtWidgets.QApplication(['-platform', 'minimal'])
  win = MainWindow()
  win.make_data_request()
  
if __name__ == '__main__':
  main_script()
