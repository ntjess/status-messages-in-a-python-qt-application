import numpy as np
from pyqtgraph.Qt import QtCore, QtWidgets

RND = np.random.default_rng()

# Inherit from QObject to be able to fire a signal
class UnstableDataSource(QtCore.QObject):
  sig_data_retrieved = QtCore.Signal(object, object) # xx, yy
  wait_time_low = 500
  wait_time_high = 3000
  timeout_limit_ratio = 0.8

  def request_data(self):
    """Either fires a signal when data is ready, or simulates a timeout"""
    wait_time = RND.integers(self.wait_time_low, self.wait_time_high)
    timeout_limit = 0.8*self.wait_time_high
    # Make some exponential data with noise
    xx = RND.uniform(-5, 5, 500)
    yy = np.exp(xx) + RND.random(xx.size)*30

    # Either return the data, or simulate a timeout
    def ret_data():
      if wait_time > timeout_limit:
        raise TimeoutError('Could not retrieve data')
      self.sig_data_retrieved.emit(xx, yy)
    actual_wait_time = int(min(timeout_limit, wait_time))
    QtCore.QTimer.singleShot(actual_wait_time, ret_data)

def non_blocking_info(msg):
  def show():
    QtWidgets.QMessageBox.information(None, 'Information', msg)
  QtCore.QTimer.singleShot(0, show)
