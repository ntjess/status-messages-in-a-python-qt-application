from pathlib import Path

import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets
from utilitys import widgets, ParamEditor, ParamEditorDockGrouping, ParamEditorPlugin

from util import UnstableDataSource, non_blocking_info


class MainWindow(QtWidgets.QMainWindow):
  def __init__(self) -> None:
    super().__init__()

    self.plot = pg.PlotWidget()
    self.scatter = pg.ScatterPlotItem()
    self.plot.addItem(self.scatter)

    self.data_source = UnstableDataSource()
    self.data_source.sig_data_retrieved.connect(self.got_data)

    # ParamEditor provides scaffolding for editable arguments to function calls
    self.tools = ParamEditor(name='Options')
    self.tools.registerFunc(self.make_data_request, name='Request Data')
    self.tools.registerFunc(self.save, name='Save Data')
    self.tools.registerFunc(self.load_from_file, name='Load Data')

    self.tools.createMenuOpt(parentMenu=self.menuBar())
    ParamEditorPlugin.addDockToWindow(self.tools, self)

    self.build_gui()

  def build_gui(self):
    widgets.EasyWidget.buildMainWin([self.plot], self)

  def make_data_request(self):
    non_blocking_info('About to request data')
    self.data_source.request_data()

  def got_data(self, xx, yy):
    print('Data retrieved!')
    self.scatter.setData(xx, yy)

  def save(self, filename=''):
    data = np.column_stack(self.scatter.getData())
    np.save(filename, data)
    print('Saved data')

  def load_from_file(self, filename=''):
    """Loads x, y data from a .npy file holding an Nx2 array"""
    print(f'Looking for file {filename}')
    xx, yy = np.load(filename).T
    self.got_data(xx, yy)
